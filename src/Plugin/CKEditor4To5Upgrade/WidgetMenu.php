<?php

namespace Drupal\ckeditor_widget_menu\Plugin\CKEditor4To5Upgrade;

use Drupal\ckeditor5\HTMLRestrictions;
use Drupal\ckeditor5\Plugin\CKEditor4To5UpgradePluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\filter\FilterFormatInterface;

/**
 * Provides the CKEditor 4 to 5 upgrade for the widgetMenu CKEditor plugin.
 *
 * @CKEditor4To5Upgrade(
 *   id = "widget_menu",
 *   cke4_buttons = {
 *     "widget_menu",
 *   },
 *   cke4_plugin_settings = {
 *   }
 * )
 */
class WidgetMenu extends PluginBase implements CKEditor4To5UpgradePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function mapCKEditor4ToolbarButtonToCKEditor5ToolbarItem(
    string $cke4_button,
    HTMLRestrictions $text_format_html_restrictions
  ): ?array {
    if ($cke4_button === 'widget_menu') {
      return ['widgetMenu'];
    }
    else {
      throw new \OutOfBoundsException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function mapCKEditor4SettingsToCKEditor5Configuration(string $cke4_plugin_id, array $cke4_plugin_settings): ?array {
    throw new \OutOfBoundsException();
  }

  /**
   * {@inheritdoc}
   */
  public function computeCKEditor5PluginSubsetConfiguration(string $cke5_plugin_id, FilterFormatInterface $text_format): ?array {
    throw new \OutOfBoundsException();
  }

}
