# Widget Menu
This plugin allows lots of widget buttons to be placed in a dropdown list so
that they don't clutter the main toolbar.

Place this button into the toolbar with other buttons. Each button in between
pipe separators will be removed from the main toolbar and added into a dropdown
menu provided by this button.

## Attribution
Widget icon by Yoyon Pujiyono from the Noun Project.
