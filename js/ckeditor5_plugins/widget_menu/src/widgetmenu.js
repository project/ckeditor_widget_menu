import { Plugin } from 'ckeditor5/src/core';

export default class WidgetMenu extends Plugin {

  static get pluginName() {
    return 'WidgetMenu';
  }

}
