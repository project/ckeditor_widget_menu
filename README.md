INTRODUCTION
------------

This module allows lots of widget buttons to be placed in a dropdown list so
that they don't clutter the CKEditor toolbar.

REQUIREMENTS
------------

### Dependencies

- drupal:ckeditor

### JavaScript Dependency

https://www.npmjs.com/package/ckeditor-widgetmenu


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.

Download the https://www.npmjs.com/package/ckeditor-widgetmenu into your
projects root `/libraries` directory. Ensure that the directory is named
`widget_menu`. Verify that the package path is correct by locating the following
`plugin.js` file at `/libraries/widget_menu/plugin.js`


CONFIGURATION
-------------

### Adding Buttons into the menu

Place the `widget_menu` button into the CKEditor toolbar within a Group
containing other buttons. Each button in the group will be removed from the main
toolbar and added into a dropdown menu.
